@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Машины</div>
                    @if (count($errors) > 0)
                        @foreach ($errors->all() as $error)
                            <div class="alert alert-danger fade in alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                                {{ $error }}
                            </div>
                        @endforeach
                    @endif
                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        <button type="button" class="btn btn-default pull-right" data-toggle="modal"
                                data-target="#myModal">Добавить новую
                        </button>
                        @include('cars/_form',compact('brands','models','years'))
                        {!! $grid !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
