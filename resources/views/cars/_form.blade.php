<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Новая машина</h4>
            </div>
            <div class="modal-body">
                {!! Form::open(array('route' => "cars_create")) !!}

                <div class="form-group error">
                    <label for="inputName" class="col-sm-3 control-label">Имя владельца</label>
                    <br>

                    <div class="col-sm-9">
                        {!! Form::text('owner_name', null, array('class'=>'input form-control', 'required', 'maxlength'=>'100','minlength'=>'3' )) !!}
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputDetail" class="col-sm-3 control-label">Цена</label>
                    <br>

                    <div class="col-sm-9">
                        {!! Form::number('price', null, array('class'=>'input form-control', 'required', 'step'=>'0.01','id'=>"car_price",'max'=>'99999999.99')) !!}
                    </div>
                </div>


                <div class="form-group">
                    <label for="inputDetail" class="col-sm-3 control-label">Год выпуска</label>
                    <br>

                    <div class="col-sm-9">
                        {!! Form::select('year_of_issue', $years,date('Y'), array('class'=>'input form-control', 'required')) !!}
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputDetail" class="col-sm-3 control-label">Марка</label>
                    <br>

                    <div class="col-sm-9">
                        {!! Form::select('car_brand_id',$brands,null, array('class'=>'input form-control', 'required')) !!}
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputDetail" class="col-sm-3 control-label">Модель</label>
                    <br>

                    <div class="col-sm-9">
                        {!! Form::select('car_model_id',$models,null, array('class'=>'input form-control', 'required')) !!}
                    </div>
                </div>
                <div class="button-action-wrap button-wrapper ">
                    <button type="submit" class=" ma-button-text btn btn-success button-main-action">Сохранить</button>

                </div>
                {!! Form::close() !!}
            </div>
            <div class="modal-footer" style="border: none!important;">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>

    </div>
</div>
