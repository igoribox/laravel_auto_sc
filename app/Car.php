<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    /**
     * cars categories const
     */
    const CATEGORY_TILL_1990 = 1;
    const CATEGORY_FROM_1990_TO_2000 = 2;
    const CATEGORY_FROM_2000_TO_2010 = 3;
    const CATEGORY_AFTER_2010 = 4;

    public $timestamps = false;

    protected $table = 'car';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'owner_name', 'price', 'car_brand_id', 'car_model_id', 'year_of_issue'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'category',
    ];

    /**
     * Set category of car based on  year_of_issue
     */
    private function setCategory()
    {
        $this->category = self::getCategoryByYear($this->year_of_issue);
    }

    public static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
            $model->setCategory();
        });

        self::updating(function ($model) {
            $model->setCategory();
        });
    }

    /**
     * Get category number based on given year
     * @param  integer $year
     * @return integer
     */
    static function getCategoryByYear($year)
    {
        $year = (int)$year;
        if ($year < 1990) {
            return self::CATEGORY_TILL_1990;
        } elseif ($year >= 1990 && $year < 2000) {
            return self::CATEGORY_FROM_1990_TO_2000;
        } elseif ($year >= 2000 && $year < 2010) {
            return self::CATEGORY_FROM_2000_TO_2010;
        } elseif ($year > 2010) {
            return self::CATEGORY_AFTER_2010;
        }
    }


    /**
     * Get category name based on category number
     * @param  integer $category
     * @return string
     */
    static function getCategoryName($category)
    {
        $category = (int)$category;
        return isset(self::getCategoriesArray()[$category]) ? self::getCategoriesArray()[$category] : '';
    }

    /**
     * @return array [[1800=>1800],[1801=>1801]] of years from 1800 to current year
     */
    static function getYearsArray()
    {
        return array_combine(range(1800, date("Y")), range(1800, date("Y")));
    }
    /**
     * @return array
     */
    static function getCategoriesArray()
    {
        return [
            self::CATEGORY_TILL_1990 => 'До 1990 года выпуска',
            self::CATEGORY_FROM_1990_TO_2000 => 'От 1990 до 2000 года выпуска',
            self::CATEGORY_FROM_2000_TO_2010 => 'От 2000 до 2010 года выпуска',
            self::CATEGORY_AFTER_2010 => 'После 2010 года выпуска',
        ];
    }
}
