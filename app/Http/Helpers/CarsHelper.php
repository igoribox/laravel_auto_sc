<?php
namespace App\Http\Helpers;

use App\Car;
use Nayjest\Grids\EloquentDataProvider;
use Nayjest\Grids\FieldConfig;
use Nayjest\Grids\Grid;
use Nayjest\Grids\GridConfig;
use Nayjest\Grids\SelectFilterConfig;


class CarsHelper
{
    static function getGridForIndexPage($brands, $models)
    {

        $query = Car::leftJoin('car_brand', 'car_brand.id', '=', 'car.car_brand_id')
            ->leftJoin('car_model', 'car_model.id', '=', 'car.car_model_id')
            ->select('car.*')
            ->addSelect('car_brand.name as car_brand_name')
            ->addSelect('car_model.name as car_model_name');

        $grid = new Grid(
            (new GridConfig)
                ->setDataProvider(
                    new EloquentDataProvider($query)
                )
                ->setName('cars')
                ->setPageSize(15)
                ->setColumns([
                        (new FieldConfig)
                            ->setName('id')
                            ->setLabel('ID')
                            ->setSortable(true),
                        (new FieldConfig)
                            ->setName('car_brand_name')
                            ->setLabel('Марка')
                            ->setSortable(true)
                            ->addFilter(
                                (new SelectFilterConfig)
                                    ->setFilteringFunc(function ($val, EloquentDataProvider $provider) {
                                        $provider->getBuilder()->where('car_brand.id', '=', $val);
                                    })
                                    ->setName('car_brand_name')
                                    ->setSubmittedOnChange(true)
                                    ->setOptions($brands)
                            ),
                        (new FieldConfig)
                            ->setName('car_model_name')
                            ->setLabel('Модель')
                            ->setSortable(true)
                            ->addFilter(
                                (new SelectFilterConfig)
                                    ->setFilteringFunc(function ($val, EloquentDataProvider $provider) {
                                        $provider->getBuilder()->where('car_model.id', '=', $val);
                                    })
                                    ->setName('car_model_name')
                                    ->setSubmittedOnChange(true)
                                    ->setOptions($models)
                            ),
                        (new FieldConfig)
                            ->setCallback(function ($val) {
                                return Car::getCategoryName($val);
                            })
                            ->setName('category')
                            ->setLabel('Категория')
                            ->setSortable(true)->addFilter(
                                (new SelectFilterConfig)
                                    ->setName('category')
                                    ->setSubmittedOnChange(true)
                                    ->setOptions(Car::getCategoriesArray())
                            ),
                        (new FieldConfig)
                            ->setName('price')
                            ->setLabel('Цена')
                            ->setSortable(true),

                    ]
                )
        );
        return $grid->render();
    }
}
