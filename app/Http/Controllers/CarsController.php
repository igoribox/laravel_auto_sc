<?php

namespace App\Http\Controllers;

use App\Car;
use App\Http\Helpers\CarsHelper;
use App\Http\Requests\CarCreateRequest;
use Illuminate\Support\Facades\DB;


class CarsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the cars dashboard.
     */
    public function index()
    {
        $brands = DB::table('car_brand')->pluck('name', 'id')->all();
        $models = DB::table('car_model')->pluck('name', 'id')->all();
        $years = Car::getYearsArray();
        $grid = CarsHelper::getGridForIndexPage($brands,$models);
        return view('cars/index', compact('grid', 'brands', 'models', 'years'));
    }
    /**
     * Create new car from request post data
     */
    public function create(CarCreateRequest $request)
    {
        $car = new Car();
        $car->create($request->all());
        return redirect()->route('cars_index');
    }
}
